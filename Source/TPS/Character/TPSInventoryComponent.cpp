// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSInventoryComponent.h"
#include "../Game/TPSGameInstance.h"

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	//Find init weaponsSlots and First Init Weapon


	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	}
	
}


// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSucsess = false;
	int8 CorrectIndex = ChangeToIndex;
	int8 step = CorrectIndex - OldIndex;
	if (CorrectIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else if (CorrectIndex < 0)
		CorrectIndex = WeaponSlots.Num() - 1;
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewWeaponInfo;
	int32 NewCurrentIndex = 0;
	do
	{
		if (WeaponSlots.IsValidIndex(CorrectIndex)) 
		{
			if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
			{
				if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
					bIsSucsess = true;
				else
				{
					UTPSGameInstance* myGui = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
					if (myGui)
					{
						FWeaponInfo myInfo;
						myGui->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);
						bool bIsFinish = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFinish)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								bIsSucsess = true;
								bIsFinish = true;
							}
							j++;
						}
					}
				}
			}
		}
		if (bIsSucsess) 
		{
			NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
			NewWeaponInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			NewCurrentIndex = CorrectIndex;
		}
		else
		{
			CorrectIndex += step;
			if (CorrectIndex > WeaponSlots.Num() - 1)
				CorrectIndex = 0;
			else if (CorrectIndex < 0)
				CorrectIndex = WeaponSlots.Num() - 1;
		}
	} while (CorrectIndex != OldIndex && !bIsSucsess);

	if (bIsSucsess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewWeaponInfo, NewCurrentIndex);
	}

	return bIsSucsess;
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::GetAdditionalInfoWeapon - No find weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::GetAdditionalInfoWeapon - Not correct weapon index - %d"), IndexWeapon);
	return result;
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			result = i;
			bIsFind = true;
		}
		i++;
	}
	return result;
}

FName UTPSInventoryComponent::GetWeaponNameByIndexSlot(int32 indexSlot)
{
	FName result;
	if (WeaponSlots.IsValidIndex(indexSlot))
		result = WeaponSlots[indexSlot].NameItem;
	return result; 
}

void UTPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;
				
				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No find weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not correct weapon index - %d"), IndexWeapon);
}

void UTPSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 ChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += ChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			OnAmmoChange.Broadcast(TypeWeapon, AmmoSlots[i].Cout);
			bIsFind = true;
		}
		i++;
	}
}

bool UTPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AviableAmmoForWeapon)
{
	bool bIsFind = false;
	int8 i = 0;
	AviableAmmoForWeapon = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon) 
		{
			bIsFind = true;
			AviableAmmoForWeapon = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
				return true;
		}
		i++;
	}
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
	return false;
}

bool UTPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
			result = true;
		i++;
	}
	return result;
}

bool UTPSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UTPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem &DropItemInfo)
{
	bool result = false;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDrophItemInfoFromInventory(IndexSlot, DropItemInfo)) {
		WeaponSlots[IndexSlot] = NewWeapon;
		if (CurrentIndexWeaponChar == IndexSlot)
			SwitchWeaponToIndex(CurrentIndexWeaponChar, CurrentIndexWeaponChar, NewWeapon.AdditionalInfo);
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		result = true;
	}
	return result;
}

bool UTPSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWaapon)
{
	int32 FreeSlot = -1;
	if (CheckCanTakeWeapon(FreeSlot))
	{
		if (WeaponSlots.IsValidIndex(FreeSlot))
		{
			WeaponSlots[FreeSlot] = NewWaapon;
			OnUpdateWeaponSlots.Broadcast(FreeSlot, NewWaapon);
			return true;
		}
	}
	return false;
}

bool UTPSInventoryComponent::GetDrophItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameByIndexSlot(IndexSlot);

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI) {
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}
	return result;
}

